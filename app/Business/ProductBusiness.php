<?php


namespace App\Business;

use App\Common\Constant;
use App\Product;

class ProductBusiness extends BaseBusiness
{
    /**
     * @var Product
     */
    protected $product;

    /**
     * ProductBusiness constructor.
     * @access public
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * Function displays a list products
     * @access public
     * @return array
     */
    public function getProducts()
    {
        try {
            return $this->product->all();

        } catch (\Exception $e) {
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

    /**
     * get list product by category
     * @param $id categoryId
     * @return Product[]|array|\Illuminate\Database\Eloquent\Collection
     */
    public function getProductByCategory($id)
    {
        try {
            return $this->product->where('category_id',$id)->get();

        } catch (\Exception $e) {
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

    /**
     * Get product detail
     * @param $id productId
     * @return array
     */
    public function getProduct($id){
        try {
            return $this->product->findOrFail($id);

        } catch (\Exception $e) {
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

}
