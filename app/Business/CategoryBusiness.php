<?php


namespace App\Business;

use App\Common\Constant;
use TCG\Voyager\Models\Category;

class CategoryBusiness extends BaseBusiness
{
    /**
     * @var Category
     */
    protected $category;

    /**
     * CategoryBusiness constructor.
     * @access public
     * @param Category $category
     */
    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Function displays a list of parent categories
     * @access public
     * @return array
     */
    public function getCategories()
    {
        try{
            return $this->category->where('parent_id',null)->get();

        }catch (\Exception $e){
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

    /**
     * Function displays a list of parent categories
     * @access public
     * @param $id
     * @return array
     */
    public function getSubCategory($id)
    {
        try{
            return $this->category->where('parent_id',$id)->get();

        }catch (\Exception $e){
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }
}
