<?php


namespace App\Business;

use App\Common\Constant;
use App\Order;
use App\OrderDetail;
use App\Product;
use Illuminate\Support\Facades\DB;

class OrderBusiness extends BaseBusiness
{
    /**
     * @var Order
     */
    protected $order, $orderDetail, $product;

    /**
     * OrderBusiness constructor.
     * @access public
     * @param Order $order
     */
    public function __construct(Order $order, OrderDetail $orderDetail, Product $product)
    {
        $this->order = $order;
        $this->orderDetail = $orderDetail;
        $this->product = $product;
    }

    /**
     * Function displays a list bill of user
     * @access public
     * @return array
     */
    public function getOrderByUser($id)
    {
        try {
            return $this->order->with('orderDetail')->where('user_id', $id)->get();

        } catch (\Exception $e) {
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

    /**
     * Function displays a list bill of user
     * @access public
     * @return array
     */
    public function saveOrder($id, $cart)
    {
        try {
            DB::beginTransaction();
            $order = $this->order->create(['user_id' => $id]);
            if (!$order) throw new \Exception('ORDER INSERT FAIL');

            foreach ($cart as $item) {
                $product = $this->product->find($item['product_id']);
                if ($this->checkProduct($item['amount'], $item['product_id'])) {
                    $this->orderDetail->create([
                        'order_id' => $order->id,
                        'product_id' => $item['product_id'],
                        'amount' => $item['amount'],
                        'price' => $product->price,
                    ]);
                    // update amount product
                    $product->amount -= $item['amount'];
                    $product->save();
                } else {
                    DB::rollBack();
                    return false;
                }
            }
            DB::commit();
            return true;

        } catch (\Exception $e) {
            DB::rollBack();
            return $this->returnFail(__('error.err_catch'), Constant::STATUS_500);
        }
    }

    /**
     * @param $amount number order customer
     * @id productId
     * @return bool
     */
    public function checkProduct($amount, $id)
    {
        $product = $this->product->find($id);
        if ($product) {
            return $product->amount >= $amount;
        } else {
            return false;
        }
    }

}
