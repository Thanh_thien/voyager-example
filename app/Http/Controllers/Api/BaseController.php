<?php

namespace App\Http\Controllers\Api;

use Illuminate\Routing\Controller;

class BaseController extends Controller
{
//    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
    public function response($message = null, $code = 200, $data = null)
    {
        $status = $code !== 200;
        $error = array();
        $error['status'] = $status;
        if ($status) {
            $error['code'] = $code;
            $error['message'] = $message;
        }
        return response()->json([
            'data' => $data,
            'error' => $error
        ], $code);
    }
}
