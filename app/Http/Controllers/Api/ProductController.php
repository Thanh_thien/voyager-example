<?php

namespace App\Http\Controllers\Api;

use App\Business\ProductBusiness;
use App\Common\Constant;

class ProductController extends BaseController
{
    /**
     * @var ProductBusiness
     */
    protected $productBus;

    /**
     * ProductController constructor.
     * @param ProductBusiness $productBus
     */
    public function __construct(ProductBusiness $productBus)
    {
        $this->productBus = $productBus;
    }

    /**
     * Get product
     * @access public
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProduct($id)
    {
        try {
            $result = $this->productBus->getProduct($id);
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

    /**
     * API a list of Product
     * @access public
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProducts()
    {
        try {
            $result = $this->productBus->getProducts();
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

    /**
     * Get Product by category
     * @param $id categoryId
     * @return \Illuminate\Http\JsonResponse
     */
    public function getProductByCategory($id){
        try {
            $result = $this->productBus->getProductByCategory($id);
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }
}
