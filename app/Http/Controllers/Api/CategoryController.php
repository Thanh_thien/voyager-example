<?php

namespace App\Http\Controllers\Api;

use App\Business\CategoryBusiness;
use App\Common\Constant;

class CategoryController extends BaseController
{
    /**
     * @var CategoryBusiness
     */
    protected $categoryBus;

    /**
     * CategoryController constructor.
     * @param CategoryBusiness $categoryBus
     */
    public function __construct(CategoryBusiness $categoryBus)
    {
        $this->categoryBus = $categoryBus;
    }

    /**
     * API list subcategories according to parent list
     * @access public
     * @return \Illuminate\Http\JsonResponse
     */
    public function getSubCategory($id)
    {
        try {
            $result = $this->categoryBus->getSubCategory($id);
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

    /**
     * API a list of parent categories
     * @access public
     * @return \Illuminate\Http\JsonResponse
     */
    public function getCategories()
    {
        try {
            $result = $this->categoryBus->getCategories();
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

}
