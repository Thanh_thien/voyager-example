<?php

namespace App\Http\Controllers\Api;

use App\Business\OrderBusiness;
use App\Common\Constant;
use Illuminate\Support\Facades\Auth;

class OrderController extends BaseController
{
    /**
     * @var OrderBusiness
     */
    protected $orderBus;

    /**
     * OrderController constructor.
     * @param OrderBusiness $orderBus
     */
    public function __construct(OrderBusiness $orderBus)
    {
        $this->orderBus = $orderBus;
    }

    /**
     * API list order of User by id
     * @access public
     * @return \Illuminate\Http\JsonResponse
     */
    public function getOrderByUser($id)
    {
        try {
            $result = $this->orderBus->getOrderByUser($id);
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

    public function saveOrderByUser(Request $request)
    {
        try {
            $result = $this->orderBus->saveOrder(auth()->id(), $request->get('cart'));
            return $this->response(__('auth.success'), Constant::STATUS_200, $result);
        } catch (\Exception $e) {
            return $this->response(__('error.err_catch'), Constant::STATUS_400);
        }
    }

}
