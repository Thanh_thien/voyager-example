<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('home');
    }
    public function detail($id){
        $product = Product::find($id);
        return view('product_detail','product');
    }
}
