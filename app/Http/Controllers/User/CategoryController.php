<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use TCG\Voyager\Models\Category;


class CategoryController extends Controller
{
    public function index()
    {
        $categoryParents = Category::where('parent_id', null)->get();
        return view('category', compact('categoryParents'));
    }

    public function subCategory($id)
    {
        $category = Category::find($id);
        $subCategories = Category::where('parent_id', $id)->get();
        return view('sub_category', compact('subCategories', 'category'));
    }

    public function detail($id){
        $category = Category::where('id',$id);
        return view('');

    }

}
