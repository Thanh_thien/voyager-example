<?php

namespace App\Http\View\Composers;

use Illuminate\View\View;
use TCG\Voyager\Models\Category;

class CategoryComposer
{
    /**
     * The user repository implementation.
     *
     * @var Category
     */
    protected $category;

    /**
     * Create a new profile composer.
     *
     * @param  Category  $category
     * @return void
     */
    public function __construct(Category $category)
    {
        // Dependencies automatically resolved by service container...
        $this->category = $category;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('categories', $this->category->where('parent_id',null)->get());
    }
}
