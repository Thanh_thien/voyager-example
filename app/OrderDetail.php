<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class OrderDetail extends Model
{
    protected $table = 'order_detail';

    protected $fillable = ['order_id', 'product_id', 'amount', 'price'];

    public $additional_attributes = ['full_name', 'total'];

    public function getFullNameAttribute()
    {
        $orderId = $this->order_id;
        $rs = '';
        $glue = ',';
        $total = 0;
        $orders = $this->getByOrder($orderId);
        foreach ($orders as $key => $orderDetail) {
            $product = $this->getProduct($orderDetail->product_id);
            $total += $orderDetail->amount * $orderDetail->price;
            $rs .= "{$product->name}: - Price: $orderDetail->price - SL: $orderDetail->amount" . $glue;
        }
        $rs .= 'Total: ' . $total;
        return $rs;
    }

    public function getByOrder($id)
    {
        return $this->where('order_id', $id)->get();
    }

    public function getProduct($id)
    {
        return Product::find($id);
    }


}
