<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    public function getProductImagesAttribute()
    {
        return json_decode($this->images);
    }
}
