<?php
namespace App\Common;

class Constant
{
    CONST STATUS_200 = 200;
    CONST STATUS_400 = 400;
    CONST STATUS_404 = 404;
    CONST STATUS_403 = 403;
    CONST STATUS_422 = 422;
    CONST STATUS_500 = 500;
}
