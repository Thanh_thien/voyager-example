<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});

Route::namespace('User')->group(function () {
    Route::get('', 'HomeController@index');
    Route::get('category', 'CategoryController@index')->name('category');
    Route::get('sub_category/{id}', 'CategoryController@subCategory')->name('subCategory');
    Route::get('product', 'ProductController@index');
    Route::get('product/{id}', 'ProductController@detail');
    Route::get('category/{id}', 'CategoryController@detail')->name('categoryDetail');


});
