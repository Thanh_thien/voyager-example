<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();

});
Route::namespace('API')->group(function () {
    Route::get('category/sub/{id}', 'CategoryController@getSubCategory');
    Route::get('category', 'CategoryController@getCategories');


    Route::get('product/category/{id}', 'ProductController@getProductByCategory');
    Route::get('product/{id}', 'ProductController@getProduct');
    Route::get('products', 'ProductController@getProducts');

    Route::get('order/user/{id}', 'OrderController@getOrderByUser');
    Route::post('order/save', 'OrderController@saveOrderByUser');

});



