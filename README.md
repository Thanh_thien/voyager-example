<p align="center"><img src="https://res.cloudinary.com/dtfbvvkyp/image/upload/v1566331377/laravel-logolockup-cmyk-red.svg" width="400"></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Installation

1. Clone Src from gitlab
    ```
    https://gitlab.com/Thanh_thien/thien_training.git
    ```
2.  Make .env and composer install
    ```bash
    $ cp .env.example .env
    $ composer install
    $ php artisan key:generate
    ```
3. Dump voyager.sql to database

## Usage
Start up a local development server with php artisan serve And, visit the URL http://localhost:8000/admin in your browser.

If you installed with the dummy data, a user has been created for you with the following login credentials:
```
email: admin@admin.com
password: password
```

