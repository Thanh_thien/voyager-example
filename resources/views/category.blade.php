@extends('master')
@section('content')
    <main id="content" class="globale-content content-page">
        <div class="container">
            <div class="brecump">
                <a href="home.html">Home</a> <img src="assets/images/arrow-right.png" alt="">
                <span>Categories</span>
            </div>
            <div class="categories">
                <h2 class="section-title">Categories</h2>
                <ul>
                    @foreach($categoryParents as $parent)
                        <li>
                            <a href="" class="cat-item image-fit">
                                <img src="{{ asset('storage/'.$parent->img) }}" alt="">
                                <h3>{{ $parent->name }}</h3>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
        </div>
    </main>
@stop
