@extends('master')
@section('content')
    <main id="content" class="globale-content content-page">
        <div class="container">
            <div class="brecump">
                <a href="home.html">Home</a> <img src="assets/images/arrow-right.png" alt="">
                <a href="categories.html">Categories</a> <img src="assets/images/arrow-right.png" alt="">
                <span>{{ $category->name }}</span>
            </div>
            <h2 class="section-title">{{ $category->name }}</h2>
            <ul class="list-inline catsub-list text-center">
                @foreach($subCategories  as $category)
                <li class="item-inline">
                    <a class="catsub-item font-bold" href="">
                        <div class="image">
                            <img src="{{ asset('storage/'.$category->img) }}" alt="">
                        </div>
                        {{$category->name}}
                    </a>
                </li>
                @endforeach
            </ul>
        </div>
    </main>
@stop
