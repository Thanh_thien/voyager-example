@extends('master')
@section('content')
    <div class="detail-slider">
        <div class="slider-item">
            <img src="assets/images/detail1.jpg" alt="">
        </div>
        <div class="slider-item">
            <img src="assets/images/detail2.jpg" alt="">
        </div>
        <div class="slider-item">
            <img src="assets/images/detail1.jpg" alt="">
        </div>
        <div class="slider-item">
            <img src="assets/images/detail1.jpg" alt="">
        </div>
        <div class="slider-item">
            <img src="assets/images/detail1.jpg" alt="">
        </div>
    </div>
    <main id="content" class="globale-content">
        <div class="container">
            <h1 class="section-title">Signature Salt Bath Treatment</h1>
            <div class="content-text">
                <p>Enjoy the full benefits of Sundara’s signature products which draw ingredients indigenous to the area. This treatment begins with exfoliation of the skin, followed by a luxurious bath soak and then full body hydration to conclude the service.</p>
            </div>
        </div>
    </main>
     <div class="suggestions">
	<div class="container">
		<h2 class="font-bold">Suggestion</h2>
		<div class="suggestions-slider">
			<ul>
                @foreach($products as $product)
				<li>
					<div class="sugges-item">
						<div class="image-fit"><img src="{{ asset('storage/'.$product->product_images[0]) }}" alt="{{$product->images}}"></div>
						<div class="info relative-section">
							<h3>{{$product->name}}</h3>
							<div class="des">{{$product->description}}</div>
							<div class="price">${{$product->price}}</div>
							<a href="" class="add-sugges">Add Suggesstion</a>
						</div>
					</div>
				</li>
                 @endforeach
			</ul>
		</div>
	</div>
</div>
{{--    <div id="addCarts">--}}
{{--        <div class="container">--}}
{{--            <div class="row">--}}
{{--                <div class="col-5">--}}
{{--                    <div class="total-price">--}}
{{--                        Total Price <b>$285.0</b>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--                <div class="col-7">--}}
{{--                    <div class="quatity text-right">--}}
{{--                        Qty:--}}
{{--                        <div class="number-custom relative-section">--}}
{{--                            <a href="" class="minus"></a>--}}
{{--                            <input type="number" name="" id="" value="1"  min="1">--}}
{{--                            <a href="" class="plus"></a>--}}
{{--                        </div>--}}
{{--                    </div>--}}
{{--                </div>--}}
{{--            </div>--}}
{{--            <button class="btn btn-default font-bold">Add to Cart</button>--}}
{{--        </div>--}}
{{--    </div>--}}
@stop
